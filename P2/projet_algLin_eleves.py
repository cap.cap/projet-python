import numpy as np
import time

def permutation(A,y,i,k) :
    line_i = A[i].copy()
    y_i = y[i]
    A[i] = A[k]
    A[k] = line_i

    y[i] = y[k]
    y[k] = y_i
    return A,y

def elimination(A,y,i,j) :
    for k in range(i+1,len(A)) :
        y[k] = y[k]-A[k,j]*y[i]/A[i,j]
        A[k] = A[k]-A[k,j]*A[i]/A[i,j]
    return A,y

def next_pivot(A,i,j) :
    n,p = A.shape
    k = i
    l = j
    while l<p and np.isclose(A[k,l],0) :
        if k < n-1 :
            k+=1
        else :
            k=i
            l+=1
    return k,l

def Gauss(A,y) :
    n,p = A.shape
    i=0
    j=0
    while i < n and j < p :
        k,j = next_pivot(A,i,j)
        if j < p :
            if k != i :
                permutation(A,y,i,k)
            A,y = elimination(A,y,i,j)
            i = i+1
            j = j+1
    return A,y

def first_non_zero(L) :
    for i in range(len(L)) :
        if not np.isclose(L[i],0) :
            return i
    return -1

def solveTriSup(A,y) :
    (n,p) = A.shape
    nb_eq = n
    res = np.zeros(p)
    for i in range(n-1,-1,-1) :
        k = first_non_zero(A[i])
        if(k != -1) :
            t = y[i]
            for l in range(k+1,p) :
                t = t-A[i,l]*res[l]
            res[k] = t/A[i,k]
        else :
            if not np.isclose(y[i],0) :
                print("Pas de solution")
                return []
            else :
                nb_eq = nb_eq-1
    return res
"""
fonction prenant en paramètre un nom de fichier 'file_name' et retournant la représentation
d'un ensemble de pages web codée dans le fichier :
- 'key_words' représente le tableau des mots-clés contenus dans les pages
- 'm_adj' représente la matrice d'adjacence du graphe formé par les liens entre les pages
"""
def get_matrix_from_file(file_name) :
    file = open(file_name,'r')
    lines = file.readlines()
    n = int(lines[0])
    key_words=[]
    m_adj=np.zeros((n,n))
    for i in range(1,n+1) :
        kws = lines[i].split()
        key_words.append(kws)
    for i in range(n+1,2*n+1) :
        l = lines[i].split()
        for j in range(1,len(l)) :
            m_adj[int(l[0]),int(l[j])] = 1
    return key_words,m_adj

"""
fonction prenant en paramètre un graphe représenté par la liste des mots clés contenus dans les pages 'k_words'
et la matrice d'adjacence 'm_adj'
"""
def affiche_graphe(k_words,m_adj) :
    for i in range(len(k_words)) :
        print("mots-clés de la page",i," :",k_words[i])
    print("matrice d'adjacence :")
    print(m_adj)

"""
fonction prenant en paramètre un tableau de liste de mots clés contenus dans les pages web 'key_words'
et une liste de mots clés recherchés 'key_words_searched'
et retournant la liste des indices des pages contenant au moins un des mots cherchés.
"""
def select_pages(key_words,key_words_searched) :
    resultat = []
    for i in range(0, len(key_words)):
        for elem in key_words[i]:
            if elem in key_words_searched and i not in resultat:
                resultat.append(i)
    return resultat

"""
fonction prenant en paramètre une matrice d'adjacence 'm_adj' et une liste d'indices de pages sélectionnées 'pages'
et retournant la sous-matrice d'adjacence correspondant aux liens entre les pages sélectionnées.
"""
def select_matrix(m_adj,pages) :
    resultat = np.zeros((len(pages), len(pages)))
    im = -1
    for i in pages:
        jm=-1
        im +=1
        for j in pages:
            jm +=1
            resultat[im, jm] = m_adj[i, j]
  
    return resultat

"""
fonction prenant en paramètre une matrice d'adjacence 'm_adj'
et retournant la matrice de transition correspondante
"""
def get_transition_matrix(m_adj) :
    m=0
    P= np.zeros((len(m_adj),len(m_adj)))
    for i in range(0, len(m_adj)):
        for j in range(0, len(m_adj)):
            m += m_adj[i,j]
        for jp in range(0, len(m_adj)):
            if m_adj[i,jp] == 0:
                P[i,jp] = 0.2*(1/len(m_adj))
            else:
                P[i,jp] = 0.8*(1/m) + 0.2*(1/len(m_adj))
        m=0
    return P

"""
fonction prenant en paramètre une matrice de transition 'm_transi' et calculant un vecteur de score r vérifiant r*mat=r
selon la méthode du premier algorithme.
"""
def page_rank1(m_transi) :
    id = np.identity(len(m_transi))
    S=[]
    N = m_transi - id
    tN = np.transpose(N)
    tN = np.concatenate((tN,np.ones((1,len(tN)))),axis=0)

    for i in range(1,len(tN)):
        S.append(0)
    S.append(1)
    tN, S = Gauss(tN, S)
    R = solveTriSup(tN, S)
    return R

"""
fonction prenant en paramètre une matrice de transition 'm_transi' ainsi qu'un seuil 'eps' et calculant un vecteur de score
r vérifiant r*mat=r selon la méthode du second algorithme.
"""
def page_rank2(m_transi,eps) :
    n = len(m_transi)
    R0 = (1/n) * np.ones((n))
    R1 = R0 @ m_transi
    while np.max(abs(R1-R0)) > eps:
        R0 = R1
        R1 = R0 @ m_transi
    return R1

"""
fonction prenant en paramètre une liste 'pages' d'indices de pages sélectionnées et un vecteur 'rank' contenant leur score
correspondant et retournant la liste des indices des pages triées dans par ordre décroissant de score.
"""
def sort_pages(pages,rank) :
    if len(pages) == len(rank) !=0 :
        ind = rank.argsort()
        res = []
        for i in ind[::-1] :
            res.append(pages[i])
        return res
    else :
        return []

def fonction_principale() :
    key_w,m_adj = get_matrix_from_file("example-graph.txt")
    # key_w,m_adj = get_matrix_from_file("example-graph2.txt")
    # key_w,m_adj = get_matrix_from_file("example-graph3.txt")
    # key_w,m_adj = get_matrix_from_file("example-graph4.txt")
    affiche_graphe(key_w,m_adj)

    print(key_w)
    print("\n\nRecherche de 'a' ou 'b' :\n----------------------")
    pages = select_pages(key_w,['a','b'])
    print("  pages sélectionnées : ",pages)
    s_m_adj = select_matrix(m_adj,pages)
    print("  matrice sélectionnée : \n",s_m_adj)
    m_transi = get_transition_matrix(s_m_adj)
    print("  matrice de transition : \n",m_transi)

    print("\n>>>>>>>>>> Méthode 1 <<<<<<<<<<<")
    start_time = time.time()
    r = page_rank1(m_transi)
    print("  rank1 trouvé : ",r)
    pages_triees_1 = sort_pages(pages,r)
    print("  pages indexées 1: ",pages_triees_1)
    print("  --- en %s seconds ---" % (time.time() - start_time))

    print("\n>>>>>>>>>> Méthode 2 <<<<<<<<<<<")

    start_time = time.time()

    r2 = page_rank2(m_transi,10**(-8))
    print("  rank2 trouvé : ",r2)
    pages_triees_2 = sort_pages(pages,r2)
    print("  pages indexées 2: ",pages_triees_2)
    print("  --- en %s seconds ---" % (time.time() - start_time))

    print("pages indexées identiques par les méthodes ? : ",pages_triees_1==pages_triees_2)

if __name__ == "__main__":
    fonction_principale()
