"""
Projet Python IUT Nantes
--- Flocon de Von Koch

Antoine RICORDEL
Capucine BECHEMIN

27/10/2020
"""

import numpy as np
import matplotlib.pyplot as plt
import math
import sys


def calcul_nouveaux_segments(x0,x1,y0,y1) : #Fonction qui renvoi les coordonées des points d'origines et des nouveaux points du segment.
    x2=(2.0*x0+x1)/3.0
    y2=(2.0*y0+y1)/3.0

    x4=(2.0*x1+x0)/3.0
    y4=(2.0*y1+y0)/3.0

    x3=(x0+x1)/2.0+(y0-y1)/(math.sqrt(3)*2)
    y3=(y0+y1)/2.0+(x1-x0)/(math.sqrt(3)*2)

    return [x0, x2, x3, x4, x1],[y0, y2, y3, y4, y1]
    #Tableaux d'abscices puis d'ordonnées des points

def transforme_liste_segments(lx,ly) : #Fonction qui met bout à bout les segments après calcul_nouveaux_segment
    if lx[2:]==[]:
        #Condition d'arrêt : il ne reste plus qu'un seul segment
        return calcul_nouveaux_segments(lx[0],lx[1],ly[0],ly[1])
        #Le premier segment
    else:
        add_lx, add_ly = transforme_liste_segments(lx[0:2], ly[0:2])
        #Prend les deux premiers points
        lx, ly = transforme_liste_segments(lx[1:], ly[1:])
        #Prend les points suivants sans le premier
        lx = add_lx + lx[1:]
        ly = add_ly + ly[1:]
        #Concatène les résultats des deux premières opérations sans le premier terme de lx et ly (évite un doublon)

        #Nous sommes conscients que la récursivité n'est pas parfaite
        #Voici la dernière tentative de return pour pallier à ce soucis mais sans succès..
        #transforme_liste_segments(lx[0:2],ly[0:2])[0] + transforme_liste_segments(lx[1:],ly[1:])[0], transforme_liste_segments(lx[0:2],ly[0:2])[1] + transforme_liste_segments(lx[1:],ly[1:])[1]
   
    return lx, ly

def von_kock(n,x,y) : #Fonction qui répète n fois la division des segments par transforme_liste_segments
    if n == 0 :
        #Cas d'arrêt : où l'indice n est nul
        return x, y
    else:
        return von_kock(n-1,transforme_liste_segments(x,y)[0],transforme_liste_segments(x,y)[1])
        #transforme_liste_segments(x,y)[0] nous donne la liste des x (de meme pour y avec [1])

def fonction_principale() :
    print("calcul_nouveaux_segments(1,2,0.5,0) :\n",calcul_nouveaux_segments(1,2,0.5,0))
    #print("([1, 1.3333333333333333, 1.6443375672974065, 1.6666666666666665, 2], [0.5, 0.33333333333333337, 0.5386751345948129, 0.16666666666666669, 0])")
    print("transforme_liste_segments([0,1,2],[0,0.5,0]) :\n",transforme_liste_segments([0,1,2],[0,0.5,0]))
    #print("([0, 0.3333333333333333, 0.35566243270259357, 0.6666666666666666, 1, 1.3333333333333333, 1.6443375672974065, 1.6666666666666665, 2], [0, 0.16666666666666666, 0.5386751345948129, 0.3333333333333333, 0.5, 0.33333333333333337, 0.5386751345948129, 0.16666666666666669, 0])")

    x, y = von_kock(4,[0,0.5,1,0],[0,math.sqrt(3)/2,0,0])
    #On a pu noter que pour n > 5 nous atteignons une limite qui rend une erreur

    plt.plot(x,y,'r-')
    plt.axis('equal')
    plt.axis('off')
    plt.show()


if __name__ == "__main__" :
    fonction_principale()
