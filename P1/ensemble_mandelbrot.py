"""
Projet Python IUT Nantes
--- Ensemble de Mandelbrot

Antoine RICORDEL
Capucine BECHEMIN

27/10/2020
"""

import matplotlib.pyplot as plt
import numpy as np

color_table = [
    (66, 30, 15),
    (25, 7, 26),
    (9, 1, 47),
    (4, 4, 73),
    (0, 7, 100),
    (12, 44, 138),
    (24, 82, 177),
    (57, 125, 209),
    (134, 181, 229),
    (211, 236, 248),
    (241, 233, 191),
    (248, 201, 95),
    (255, 170, 0),
    (204, 128, 0),
    (153, 87, 0),
    (106, 52, 3)]

def prochains_termes(x,y,a,b) : #Fonction qui renvoi les termes suivant de la suite ayant pour paramètre a et b
    a = a + pow(x,2) - pow(y,2) 
    #a est ici une variable temporaire pour ne pas écraser x
    y = b + 2*x*y
    x = a
    return x, y

def indice_divergence(x,y,a,b,n) : #Fonction qui retourne l'indice de divergence de la suite
    if (n>127):                    
        #On fixe une limite de n à 127
        return -1
    elif(pow(x,2)+pow(y,2) > 4):
        return n
    else:
        return indice_divergence(prochains_termes(x, y, a, b)[0], prochains_termes(x, y, a, b)[1], a, b, n+1)
        #x et y prennent la valeurs des sorties 0 et 1 de la fonction prochains_termes

def get_color(a,b) : #Fonction qui récupere la couleur des pixels
    n = indice_divergence(0,0,a,b,0) 
    #On va chercher l'indice de divergence de la suite pour déterminer sa couleur
    if n == -1:
        return (0,0,0)
    else:
        return color_table[n%len(color_table)] 
        #La couleur modulo n de la longueur de la table des couleurs initialisée en début de code

def create_image(x,y,l,h,nb_px,nb_py) : #Fonction qui crée l'image finale
    res = np.zeros((nb_px, nb_py, 3), dtype=np.uint8)
    for i in range(nb_px) :
        for j in range(nb_py) :
            res[i][j] = get_color(x+i*l/nb_px,y+j*h/nb_py)
            #Traitement de la couleur de chaque pixel
    return res

def fonction_principale() :
    print("prochains_termes(3,4,1,2) :",prochains_termes(3,4,1,2)," doit valoir (-6, 26)")
    print("prochains_termes(0.5,0.1,1,3) :",prochains_termes(0.5,0.1,1,3)," doit valoir (1.24, 3.1)")
    print("indice_divergence(0,0,0.5,0.5,0) :",indice_divergence(0,0,0.5,0.5,0)," doit valoir 5")
    print("indice_divergence(0,0,0.01,0.1,0) :",indice_divergence(0,0,0.01,0.1,0)," doit valoir -1")
    print("indice_divergence(0,0,-0.24,-0.9,0) :",indice_divergence(0,0,-0.24,-0.9,0)," doit valoir 8")

    img = create_image(-0.58,0.626,0.03,0.03,500,500)
    # img = create_image(-0.7,0.5,0.3,0.3,500,500)
    # img = create_image(-2.,-1.25,2.5,2.5,500,500)

    plt.imshow(img)
    plt.axis("equal")
    plt.axis('off')
    plt.show() #Affichage de l'image


if __name__ == "__main__":
    fonction_principale()
