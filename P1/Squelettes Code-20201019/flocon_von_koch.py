import numpy as np
import matplotlib.pyplot as plt
import math
import sys


def calcul_nouveaux_segments(x0,x1,y0,y1) :
    # à compléter
    return [x0, x1],[y0, y1]

def transforme_liste_segments(lx,ly) :
    # à compléter
    return lx,ly

def von_kock(n,x,y) :
    # à compléter
    return x,y

def fonction_principale() :
    print("calcul_nouveaux_segments(1,2,0.5,0) :\n",calcul_nouveaux_segments(1,2,0.5,0))
    print("([1, 1.3333333333333333, 1.6443375672974065, 1.6666666666666665, 2], [0.5, 0.33333333333333337, 0.5386751345948129, 0.16666666666666669, 0])")
    print("transforme_liste_segments([0,1,2],[0,0.5,0]) :\n",transforme_liste_segments([0,1,2],[0,0.5,0]))
    print("([0, 0.3333333333333333, 0.35566243270259357, 0.6666666666666666, 1, 1.3333333333333333, 1.6443375672974065, 1.6666666666666665, 2], [0, 0.16666666666666666, 0.5386751345948129, 0.3333333333333333, 0.5, 0.33333333333333337, 0.5386751345948129, 0.16666666666666669, 0])")

    x,y = von_kock(4,[0,0.5,1,0],[0,math.sqrt(3)/2,0,0])
    plt.plot(x,y,'r-')
    plt.axis('equal')
    plt.axis('off')
    plt.show()


if __name__ == "__main__" :
    fonction_principale()